﻿using System;
using System.ComponentModel;
using VPNSwitcher.CustomLocalization.CustomLocalizationResources;

namespace VPNSwitcher.CustomLocalization
{
    [AttributeUsage(
        AttributeTargets.Assembly | AttributeTargets.Module | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field |
        AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Parameter | AttributeTargets.Delegate | AttributeTargets.ReturnValue | AttributeTargets.GenericParameter)]
    public class CustomLocalizedDescriptionAttribute : DescriptionAttribute
    {
        public CustomLocalizedDescriptionAttribute(string key)
            : base(Localize(key))
        {
        }

        private static string Localize(string key)
        {
            var rm = CustomCommon.ResourceManager;
            return rm.GetString(key);
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
    public class CustomLocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        public CustomLocalizedDisplayNameAttribute(string key)
            : base(Localize(key))
        {
        }

        private static string Localize(string key)
        {
            var rm = CustomCommon.ResourceManager;
            return rm.GetString(key);
        }
    }

    [AttributeUsage(
        AttributeTargets.Assembly | AttributeTargets.Module | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field |
        AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Parameter | AttributeTargets.Delegate | AttributeTargets.ReturnValue | AttributeTargets.GenericParameter)]
    public class CustomLocalizedCategoryAttribute : CategoryAttribute
    {
        public CustomLocalizedCategoryAttribute(string key) : base(key)
        {
        }

        protected override string GetLocalizedString(string value)
        {
            var rm = CustomCommon.ResourceManager;
            return rm.GetString(value);
        }
    }
}