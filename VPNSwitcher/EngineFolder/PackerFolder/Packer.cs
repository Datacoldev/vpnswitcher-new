﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace VPNSwitcher.EngineFolder.PackerFolder
{
    public class Packer
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private class MEMORYSTATUSEX
        {
            public uint dwLength;
            public uint dwMemoryLoad;
            public ulong ullTotalPhys;
            public ulong ullAvailPhys;
            public ulong ullTotalPageFile;
            public ulong ullAvailPageFile;
            public ulong ullTotalVirtual;
            public ulong ullAvailVirtual;
            public ulong ullAvailExtendedVirtual;
            public MEMORYSTATUSEX()
            {
                dwLength = (uint)Marshal.SizeOf(typeof(MEMORYSTATUSEX));
            }
        }


        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool GlobalMemoryStatusEx([In, Out] MEMORYSTATUSEX lpBuffer);

        public string EncodingKey;
        public ShortHeaderClass ShortHeader;
        //Old windows header for packages
        public static string OldPackageHeader = "BEEZZYUPDATE";
        //New crossplatform header for packages
        public static string NewPackageHeader = "HUBSCUREPACK";
        public string RootFolder = "";

        public event EventHandler<PackerEventArgs> OnExtractFile;

        public class Header
        {

            public Int32 TotalEntries { get; set; }
            public Int64[] EntriesSize
            {
                get { return EntriesSizeList.ToArray(); }
            }
            private List<Int64> EntriesSizeList { get; set; }
            public Header()
            {
                TotalEntries = 0;
                EntriesSizeList = new List<Int64>();
            }
            public void AddEntrySize(Int64 newSize)
            {
                EntriesSizeList.Add(newSize);
            }
        }

        public static byte[] SerializeToByteArray(object obj)
        {
            if (obj == null)
            {
                return null;
            }

            var serializer = new XmlSerializer(obj.GetType());
            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter writer = new StreamWriter(memoryStream))
                {
                    serializer.Serialize(writer, obj);
                }
                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="byteArray"></param>
        /// <returns></returns>
        public static T Deserialize<T>(byte[] byteArray) where T : class
        {
            if (byteArray == null)
            {
                return null;
            }
            using (var memoryStream = new MemoryStream())
            {
                var binaryFormatter = new BinaryFormatter();
                memoryStream.Write(byteArray, 0, byteArray.Length);
                memoryStream.Seek(0, SeekOrigin.Begin);
                var obj = (T)binaryFormatter.Deserialize(memoryStream);
                return obj;
            }
        }


        public static T Deserialize2<T>(byte[] byteArray) where T : class
        {
            if (byteArray == null)
            {
                return null;
            }

            using (var memoryStream = new MemoryStream())
            {
                var xmlSerializer = new XmlSerializer(typeof(T));
                memoryStream.Write(byteArray, 0, byteArray.Length);
                memoryStream.Seek(0, SeekOrigin.Begin);
                var obj = (T)xmlSerializer.Deserialize(memoryStream);
                return obj;
            }
        }


        public static string SerializeToString<T>(T toSerialize)
        {
            string retval;
            var xmlSerializer = new XmlSerializer(toSerialize.GetType());
            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                retval = textWriter.ToString();
            }
            return retval;
        }


        public static T DeserializeFromString<T>(string toDeserialize)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var textReader = new StringReader(toDeserialize))
            {
                return (T)xmlSerializer.Deserialize(textReader);
            }
        }

        /// <summary>
        /// Type of package
        /// </summary>
        public enum PackageType
        {
            NotPackage,
            OldPackage,
            NewPackage
        }

        [Serializable]
        public enum ActivePackageRunTypeEnum
        {
            Sheduled,
            OnBrowserStart,
            OnBrowserClose,
            OnEngineStart,
            OnEngineClose,
            ManualRun
        }

        [Serializable]
        public class InternalDescriptionClass
        {
            public bool IsExecutable { get; set; }
            public string SVGButton { get; set; }
            public string SVGButtonLabel { get; set; }
            public string ExecutableName { get; set; }
            public string Arguments { get; set; }
            public ActivePackageRunTypeEnum ActivePackageRunType { get; set; }
            public bool AllowRunByUser { get; set; }
            public int ScheduledRunEach { get; set; }
            public bool ThisPackageOn { get; set; }
            public string Serial { get; set; }
            public bool AdminRights { get; set; }
            public bool AlternativeWait { get; set; }
        }

        [Serializable]
        public class ShortHeaderClass
        {
            public string PackageName { get; set; }
            public string PackageFileName { get; set; }
            public string Description { get; set; }
            public string Serial { get; set; }
            public List<ShortHeaderClass> Headers;

            public ShortHeaderClass(string packageName, string packageFileName, string description, string serial)
            {
                PackageName = packageName;
                PackageFileName = packageFileName;
                Description = description;
                Serial = serial;
                Headers = new List<ShortHeaderClass>();
            }

            public ShortHeaderClass()
            {
                PackageName = "";
                PackageFileName = "";
                Description = "";
                Serial = "";
                Headers = new List<ShortHeaderClass>();
            }
        }

        public class Item
        {
            public Byte[] RawData { get; set; }
            public String Name { get; set; }
            public String RelativeUri { get; set; }
            public DateTime TimeStamp { get; set; }

            public Int64 ItemSize
            {
                get
                {
                    Int64 retVal = 4; //Name.Lenght;
                    retVal += Name.Length;
                    retVal += 4; //RelativeUri.Length
                    retVal += RelativeUri.Length;
                    retVal += RawData.Length;
                    return retVal;
                }
            }

            public Byte[] SerializedData
            {
                get
                {
                    var retVal = new List<Byte>();
                    retVal.AddRange(BitConverter.GetBytes(Name.Length));
                    retVal.AddRange(Encoding.Default.GetBytes(Name));
                    retVal.AddRange(BitConverter.GetBytes(RelativeUri.Length));
                    retVal.AddRange(Encoding.Default.GetBytes(RelativeUri));
                    retVal.AddRange(RawData);
                    return retVal.ToArray();
                }
            }

            public Item()
            {
                RawData = new Byte[0];
                Name = String.Empty;
                RelativeUri = String.Empty;
                TimeStamp = DateTime.MinValue;
            }

            public Item(Byte[] serializedItem)
            {
                var cursor = 0;
                var nl = BitConverter.ToInt32(serializedItem, cursor);
                cursor += 4;
                Name = Encoding.Default.GetString(serializedItem, cursor, nl);
                cursor += nl;
                //We have timestamp
                if (Name.Contains("*"))
                {
                    var splittedName = Name.Split(new char[] { '*' });
                    Name = splittedName[0];
                    TimeStamp = DateTime.ParseExact(splittedName[1], "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'", CultureInfo.InvariantCulture);
                }
                var rl = BitConverter.ToInt32(serializedItem, cursor);
                cursor += 4;
                RelativeUri = Encoding.Default.GetString(serializedItem, cursor, rl);
                cursor += rl;
                RawData = new Byte[serializedItem.Length - cursor];
                Array.Copy(serializedItem, cursor, RawData, 0, RawData.Length);
            }
        }

        public FileInfo PackedFile { get; private set; }
        public List<Item> Data { get; private set; }

        public Header FileHeaderDefinition { get; private set; }

        public Packer(String fileName)
        {
            PackedFile = new FileInfo(fileName);
            FileHeaderDefinition = new Header();
            Data = new List<Item>();
            ShortHeader = new ShortHeaderClass();
        }

        public bool PackFolderContent(String folderFullName)
        {
            var di = new DirectoryInfo(folderFullName);

            //Think about setting up strong checks and errors trapping

            if (di.Exists)
            {
                var files = di.GetFiles("*", SearchOption.AllDirectories);
                foreach (var fi in files)
                {
                    var it = GetItem(fi, di.FullName);
                    if (it != null)
                    {
                        Data.Add(it);
                        FileHeaderDefinition.TotalEntries++;
                        FileHeaderDefinition.AddEntrySize(it.ItemSize);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Unpack all content to folder
        /// </summary>
        /// <param name="rootFolder"></param>
        /// <returns></returns>
        public bool UnpackAllToFolder(string rootFolder)
        {
            var retval = false;
            Parallel.ForEach(Data, (item) =>
            {
                var relativeFileName = item.RelativeUri.Replace("/", "\\").TrimStart(new char[] { '\\' });
                var fileName = Path.Combine(rootFolder, relativeFileName);
                var targetPath = Path.GetDirectoryName(fileName);
                if (!Directory.Exists(targetPath))
                {
                    Directory.CreateDirectory(targetPath);
                }
                File.WriteAllBytes(fileName, item.RawData);
                if (OnExtractFile != null) { OnExtractFile.Invoke(this, new PackerEventArgs(fileName)); }
            });
            return retval;
        }

        public void DeleteDirectory(string target_dir)
        {
            var files = Directory.GetFiles(target_dir);
            var dirs = Directory.GetDirectories(target_dir);
            foreach (var file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }
            foreach (var dir in dirs)
            {
                DeleteDirectory(dir);
            }
            Directory.Delete(target_dir, false);
        }

        public void AddSingleVirtualFile(string fileName, byte[] fileContent)
        {
            var it = GetVirtualItem(fileName, fileContent, DateTime.Now);
            Data.Add(it);
            FileHeaderDefinition.TotalEntries++;
            FileHeaderDefinition.AddEntrySize(it.ItemSize);
        }

        public void AddSingleFile(string fileName)
        {
            var fi = new FileInfo(fileName);
            var di = new DirectoryInfo(Path.GetDirectoryName(fileName));
            var it = GetItem(fi, di.FullName);
            Data.Add(it);
            FileHeaderDefinition.TotalEntries++;
            FileHeaderDefinition.AddEntrySize(it.ItemSize);
        }

        private Item GetVirtualItem(string nameForFile, byte[] virtualFileContent, DateTime timestamp)
        {
            var retVal = new Item()
            {
                //Name = sourceFile.Name,
                Name = String.Format("{0}*{1}", nameForFile, timestamp.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'", CultureInfo.InvariantCulture)),
                RelativeUri = nameForFile,
                TimeStamp = timestamp.ToUniversalTime(),
                RawData = virtualFileContent
            };
            return retVal;
        }

        private Item SetItem(FileInfo sourceFile, String packedRoot, DateTime timestamp)
        {
            var retval = GetItem(sourceFile, packedRoot);
            retval.Name = String.Format("{0}*{1)}", retval.Name, timestamp.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'", CultureInfo.InvariantCulture));
            retval.TimeStamp = timestamp;
            return retval;
        }

        private Item GetItem(FileInfo sourceFile, String packedRoot)
        {
            if (!String.IsNullOrEmpty(RootFolder))
            {
                packedRoot = RootFolder;
            }
            if (sourceFile.Exists)
            {
                var retVal = new Item()
                {
                    //Name = sourceFile.Name,
                    Name = String.Format("{0}*{1}", sourceFile.Name, sourceFile.LastWriteTime.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'", CultureInfo.InvariantCulture)),
                    RelativeUri = sourceFile.FullName.Substring(packedRoot.Length).Replace("\\", "/"),
                    TimeStamp = sourceFile.LastWriteTime.ToUniversalTime(),
                    RawData = File.ReadAllBytes(sourceFile.FullName)
                };
                return retVal;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get memory information
        /// </summary>
        /// <param name="TotalPhys">Total physical memory</param>
        /// <param name="AvailPhys">Aviabile physical memory</param>
        /// <returns></returns>
        private bool GetTotalPhysicalMemory(out ulong TotalPhys, out ulong AvailPhys)
        {
            var retval = false;
            TotalPhys = 0;
            AvailPhys = 0;
            var msex = new MEMORYSTATUSEX();
            retval = GlobalMemoryStatusEx(msex);
            if (retval)
            {
                TotalPhys = msex.ullTotalPhys;
                AvailPhys = msex.ullAvailPhys;
            }
            return retval;
        }

        public void Save(string file)
        {
            if (File.Exists(file))
            {
                File.Delete(file);
                System.Threading.Thread.Sleep(100);
            }
            using (var fs = new FileStream(file, FileMode.CreateNew, FileAccess.Write))
            {
                //Writing package ID
                var header = Encoding.UTF8.GetBytes(NewPackageHeader);
                fs.Write(header, 0, header.Length);

                var shortHeaderBytes = SerializeToByteArray(ShortHeader);
                fs.Write(BitConverter.GetBytes(shortHeaderBytes.Length), 0, 4);
                fs.Write(shortHeaderBytes, 0, shortHeaderBytes.Length);

                //Writing Header
                //4 bytes
                fs.Write(BitConverter.GetBytes(FileHeaderDefinition.TotalEntries), 0, 4);
                //8 bytes foreach size
                foreach (var size in FileHeaderDefinition.EntriesSize)
                {
                    fs.Write(BitConverter.GetBytes(size), 0, 8);
                }
                foreach (var it in Data)
                {
                    fs.Write(it.SerializedData, 0, it.SerializedData.Length);
                }
                fs.Close();
            }
        }

        public void Save()
        {
            if (PackedFile.Exists)
            {
                PackedFile.Delete();
                System.Threading.Thread.Sleep(100);
            }
            //Save all to temp  file
            var tempFile = Path.GetTempFileName();
            //using (FileStream fs = new FileStream(PackedFile.FullName, FileMode.CreateNew, FileAccess.Write))
            using (var fs = new FileStream(tempFile, FileMode.OpenOrCreate, FileAccess.Write))
            {
                //Writing package ID
                var header = Encoding.UTF8.GetBytes(NewPackageHeader);
                fs.Write(header, 0, header.Length);
                var shortHeaderBytes = SerializeToByteArray(ShortHeader);
                fs.Write(BitConverter.GetBytes(shortHeaderBytes.Length), 0, 4);
                fs.Write(shortHeaderBytes, 0, shortHeaderBytes.Length);
                //Writing Header
                //4 bytes
                fs.Write(BitConverter.GetBytes(FileHeaderDefinition.TotalEntries), 0, 4);
                //8 bytes foreach size
                foreach (var size in FileHeaderDefinition.EntriesSize)
                {
                    fs.Write(BitConverter.GetBytes(size), 0, 8);
                }
                foreach (var it in Data)
                {
                    fs.Write(it.SerializedData, 0, it.SerializedData.Length);
                }
                fs.Close();
            }
            //Compress
            using (var fs = new FileStream(tempFile, FileMode.Open, FileAccess.Read))
            {
                using (var compressedFileStream = File.Create(PackedFile.FullName))
                {
                    using (var compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
                    {
                        fs.CopyTo(compressionStream);
                    }
                }
                fs.Close();
            }
            //Delete tempt file
            File.Delete(tempFile);
        }

        public void Close()
        {
            Data.Clear();
        }

        /// <summary>
        /// Remove item by name
        /// </summary>
        /// <param name="itemName">Item name</param>
        public void RemoveItemByName(string itemName)
        {
            var index = -1;
            foreach (var item in Data)
            {
                if (itemName == item.Name)
                {
                    index = Data.IndexOf(item);
                    break;
                }
            }
            if (index > -1)
            {
                Data.RemoveAt(index);
            }
        }

        /// <summary>
        /// Get item by name
        /// </summary>
        /// <param name="itemName">Item name</param>
        /// <returns></returns>
        public byte[] GetItemByName(string itemName)
        {
            byte[] retval = null;
            foreach (var item in Data)
            {
                if (itemName == item.Name)
                {
                    retval = item.RawData;
                    break;
                }
            }
            return retval;
        }

        public bool Open()
        {
            var retval = true;
            if (PackedFile.Exists)
            {
                //Decompress to temp file
                var tempFile = Path.GetTempFileName();
                using (var fs = new FileStream(PackedFile.FullName, FileMode.Open, FileAccess.Read))
                {
                    using (var decompressedFileStream = File.Create(tempFile))
                    {
                        using (var decompressionStream = new GZipStream(fs, CompressionMode.Decompress))
                        {
                            decompressionStream.CopyTo(decompressedFileStream);
                        }
                    }
                }
                //Open from temp file
                using (var fs = new FileStream(tempFile, FileMode.Open, FileAccess.Read))
                {
                    try
                    {
                        //read header
                        //Read package ID
                        var oldHeader = new byte[Encoding.UTF8.GetBytes(OldPackageHeader).Length];
                        fs.Read(oldHeader, 0, oldHeader.Length);
                        var header = Encoding.UTF8.GetString(oldHeader);
                        var packageType = PackageType.NotPackage;
                        if (header == OldPackageHeader) { packageType = PackageType.OldPackage; }
                        if (header == NewPackageHeader) { packageType = PackageType.NewPackage; }

                        if (packageType == PackageType.NotPackage)
                        {
                            fs.Close();
                            File.Delete(tempFile);
                            retval = false;
                        }
                        else
                        {
                            //Read short header
                            var readBuffer = new Byte[4];
                            fs.Read(readBuffer, 0, readBuffer.Length);
                            var shortHeaderSize = BitConverter.ToInt32(readBuffer, 0);
                            readBuffer = new Byte[shortHeaderSize];
                            fs.Read(readBuffer, 0, shortHeaderSize);
                            if (packageType == PackageType.OldPackage)
                            {
                                ShortHeader = Deserialize<ShortHeaderClass>(readBuffer);
                            }
                            if (packageType == PackageType.NewPackage)
                            {
                                //Deserialize new format
                                ShortHeader = Deserialize2<ShortHeaderClass>(readBuffer);
                            }
                            readBuffer = new Byte[4];
                            fs.Read(readBuffer, 0, readBuffer.Length);
                            FileHeaderDefinition.TotalEntries = BitConverter.ToInt32(readBuffer, 0);
                            for (var i = 0; i < FileHeaderDefinition.TotalEntries; i++)
                            {
                                readBuffer = new Byte[8];
                                fs.Read(readBuffer, 0, readBuffer.Length);
                                FileHeaderDefinition.AddEntrySize(BitConverter.ToInt64(readBuffer, 0));
                            }

                            foreach (var size in FileHeaderDefinition.EntriesSize)
                            {
                                readBuffer = new Byte[size];
                                fs.Read(readBuffer, 0, readBuffer.Length);
                                Data.Add(new Item(readBuffer));
                            }
                        }
                        //fs.Close();
                    }
                    catch
                    {
                        //Do nothing
                    }
                }
                //Delete temp file
                File.Delete(tempFile);
            }
            else
            {
                retval = false;
            }
            return retval;
        }
    }
}
