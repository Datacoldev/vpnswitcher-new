using System;

namespace VPNSwitcher.EngineFolder.PackerFolder
{
    public class PackerEventArgs : EventArgs
    {
        public string FileName;
        public PackerEventArgs(string fileName)
        {
            FileName = fileName;
        }
    }
}