﻿using MAPrimitives.EngineFolder;
using MAPrimitives.EngineMainPrimitivesFactoryFolder;
using MAPrimitives.OptionFolder;

namespace VPNSwitcher.EngineFolder
{
    public class CustomEngineMainPrimitivesFactory : EngineMainPrimitivesFactory
    {
        public override IEngine CreateEngine()
        {
            return new CustomEngine();
        }

        public override Options GenerateOptionObject()
        {
            return new CustomOptions();
        }
    }
}