﻿using System;
using System.Collections.Generic;
using MAPrimitives.QueueFolder.FillQueueBehaviourFolder;
using MAPrimitives.QueueFolder.QueueItemFolder;

namespace VPNSwitcher.EngineFolder
{
    public class CustomEngineInputQueueBehavior : IFillInputQueueBehaviour
    {
        private readonly CustomEngine CustomEngine;

        public CustomEngineInputQueueBehavior(CustomEngine customEngine)
        {
            CustomEngine = customEngine;
        }

        public void Fill()
        {
            var itemList = new List<QueueItemBase>();

            if ((CustomEngine.OptionWrapper.Options as CustomOptions) == null)
            {
                throw new Exception("Исходные данные пустые");
            }
        }
    }
}