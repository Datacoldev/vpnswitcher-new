﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace VPNSwitcher.EngineFolder
{
    public class OpenVpnManager : IDisposable
    {
        public enum Signal
        {
            Hup,
            Term,
            Usr1,
            Usr2
        }

        private readonly Socket _socket;
        private const int BufferSize = 1024;
        private readonly string _ovpnFileName;
        private const string EventName = "MyOpenVpnEvent";
        private readonly Process _ovpnProcess = new Process();
        private readonly string _openVpnExePath;
        private readonly bool _connectManagementInterface;
        private readonly bool _makeLog;

        public int ProcessId;
        public Process OpenVpnProcess;

        private void CopyAuthorizationFiles(string sourceFolder, string targetFolder)
        {
            CopyFiles(sourceFolder, targetFolder, "*.crt");
            CopyFiles(sourceFolder, targetFolder, "*.key");
        }

        private void CopyFiles(string sourceFolder, string targetFolder, string ext)
        {
            foreach (var sourceFilePath in Directory.GetFiles(sourceFolder, ext))
            {
                var fileName = Path.GetFileName(sourceFilePath);
                if (fileName == null) continue;

                var destinationFilePath = Path.Combine(targetFolder, fileName);
                File.Copy(sourceFilePath, destinationFilePath, true);
            }
        }

        private void RunOpenVpnProcess()
        {
            _ovpnProcess.StartInfo.CreateNoWindow = true;
            _ovpnProcess.StartInfo.UseShellExecute = false;
            _ovpnProcess.EnableRaisingEvents = true;

            var openVpnFolder = Path.GetDirectoryName(_openVpnExePath);
            var authFolder = Path.GetDirectoryName(_ovpnFileName);
            
            CopyAuthorizationFiles(authFolder, openVpnFolder);

            if (openVpnFolder != null)
            {
                var logFile = Path.Combine(openVpnFolder, "log.txt");
                var logString = "";
                if (_makeLog)
                {
                    logString = String.Format(" --log \"{0}\"", logFile);
                }
                _ovpnProcess.StartInfo.Arguments = String.Format("--config \"{0}\" --service {1} 0{2}", _ovpnFileName, EventName, logString);

                _ovpnProcess.StartInfo.FileName = _openVpnExePath;
                _ovpnProcess.StartInfo.WorkingDirectory = openVpnFolder;
                //Uncomment for debug
                //File.WriteAllText("vpn.bat", $"{openVpnExePath} {ovpnProcess.StartInfo.Arguments}");
                OpenVpnProcess = _ovpnProcess;
                _ovpnProcess.Start();
                ProcessId = _ovpnProcess.Id;
            }
        }

        public OpenVpnManager(string host, int port, string ovpnFileName, string userName, string password, string openVpnExeFileName, bool connectManagementInterface, bool makeLog)
        {
            _connectManagementInterface = connectManagementInterface;
            _makeLog = makeLog;
            _openVpnExePath = openVpnExeFileName;
            if (!string.IsNullOrEmpty(ovpnFileName))
            {
                if (!Path.IsPathRooted(ovpnFileName))
                {
                    _ovpnFileName = Path.Combine(Directory.GetCurrentDirectory(), ovpnFileName);
                }
                var ovpnFileContent = File.ReadAllLines(ovpnFileName);
                //management
                var idx = Array.FindIndex(ovpnFileContent, x => x.StartsWith("management"));
                if (idx >= 0)
                {
                    ovpnFileContent[idx] = string.Format("management {0} {1}", host, port);
                }
                else
                {
                    var lastIdx = ovpnFileContent.Length - 1;
                    var lastLine = ovpnFileContent[lastIdx];
                    ovpnFileContent[lastIdx] = String.Format("{0}{1}management {2} {3}", lastLine, Environment.NewLine, host, port);
                }

                //auto login
                var idx2 = Array.FindIndex(ovpnFileContent, x => x.StartsWith("auth-user-pass"));
                if (idx2 >= 0)
                {
                    if (userName == null || password == null)
                    {
                        throw new ArgumentException("Username or password cannot be null");
                    }
                    // create a credentials file
                    var passFileName = Path.Combine(Path.GetTempPath(), "ovpnpass.txt").Replace(@"\", @"\\");
                    File.WriteAllLines(passFileName, new[] { userName, password });
                    // add its path the ovpn file and write it back to the ovpn file
                    ovpnFileContent[idx2] = String.Format("auth-user-pass \"{0}\"", passFileName);
                }
                else
                {
                    if (!string.IsNullOrEmpty(userName) || !string.IsNullOrEmpty(password))
                    {
                        throw new ArgumentException("Username or password are provided but the *.ovpn file doesn't have the line 'auth-user-pass'");
                    }
                }
                File.WriteAllLines(ovpnFileName, ovpnFileContent);
                _ovpnFileName = ovpnFileName;
                RunOpenVpnProcess();
            }
            if (_connectManagementInterface)
            {
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _socket.Connect(host, port);
                SendGreeting();
            }
        }

        #region Commands
        public string GetStatus()
        {
            return SendCommand("status");
        }

        public string GetState()
        {
            return SendCommand("state");
        }

        public string GetState(int n)
        {
            return SendCommand(string.Format("state {0}", n));
        }

        public string GetStateAll()
        {
            return SendCommand("state all");
        }

        public string SetStateOn()
        {
            return SendCommand("state on");
        }

        public string SetStateOnAll()
        {
            return SendCommand("state on all");
        }

        public string GetStateOff()
        {
            return SendCommand("state off");
        }

        public string GetVersion()
        {
            return SendCommand("version");
        }

        public string GetPid()
        {
            return SendCommand("pid");
        }

        public string SendSignal(Signal sgn)
        {
            return SendCommand(string.Format("signal SIG{0}", sgn.ToString().ToUpper()));
        }

        public string Mute()
        {
            return SendCommand("pid");
        }

        public string GetEcho()
        {
            return SendCommand("echo");
        }

        public string GetHelp()
        {
            return SendCommand("help");
        }

        public string Kill(string name)
        {
            return SendCommand(string.Format("kill {0}", name));
        }

        public string Kill(string host, int port)
        {
            return SendCommand(string.Format("kill {0}:{1}", host, port));
        }

        public string GetNet()
        {
            return SendCommand("net");
        }

        public string GetLogAll()
        {
            return SendCommand("state off");
        }

        public string SetLogOn()
        {
            return SendCommand("log on");
        }

        public string SetLogOnAll()
        {
            return SendCommand("log on all");
        }

        public string SetLogOff()
        {
            return SendCommand("log off");
        }

        public string GetLog(int n = 1)
        {
            return SendCommand(string.Format("log {0}", n));
        }

        public string SetHoldOff()
        {
            return SendCommand("hold off");
        }

        public string ReleaseHold()
        {
            return SendCommand("hold release");
        }
        public string GetHold()
        {
            return SendCommand("hold");
        }

        public string SendMalCommand()
        {
            return SendCommand("fdsfds");
        }

        public static string TreamRetrievedString(string s)
        {
            return s.Replace("\0", "");
        }

        private void SendGreeting()
        {
            var bf = new byte[BufferSize];
            var rb = _socket.Receive(bf, 0, bf.Length, SocketFlags.None);
            if (rb < 1)
            {
                throw new SocketException();
            }
        }
        #endregion

        private string SendCommand(String cmd)
        {
            if (!_connectManagementInterface) { return ""; }
            var bf = new byte[BufferSize];
            var sb = new StringBuilder();
            try
            {
                _socket.Send(Encoding.Default.GetBytes(cmd + "\r\n"));
                while (true)
                {
                    Thread.Sleep(100);
                    var rb = _socket.Receive(bf, 0, bf.Length, 0);
                    var str = Encoding.UTF8.GetString(bf).Replace("\0", "");
                    if (rb < bf.Length)
                    {
                        if (str.Contains("\r\nEND"))
                        {
                            var a = str.Substring(0, str.IndexOf("\r\nEND", StringComparison.Ordinal));
                            sb.Append(a);
                        }
                        else if (str.Contains("SUCCESS: "))
                        {
                            var a = str.Replace("SUCCESS: ", "").Replace("\r\n", "");
                            sb.Append(a);
                        }
                        else if (str.Contains("ERROR: "))
                        {
                            var msg = str.Replace("ERROR: ", "").Replace("\r\n", "");
                            throw new ArgumentException(msg);
                        }
                        else
                        {
                            //todo
                            continue;
                        }

                        break;
                    }
                    
                    sb.Append(str);
                }
            }
            catch
            {
                //Do nothing
            }
            return sb.ToString();
        }

        public void Dispose()
        {
            if (_socket != null)
            {
                if (_ovpnFileName != null)
                {
                    SendSignal(Signal.Term);
                }
                _socket.Close();
            }
            try
            {
                _ovpnProcess.Kill();
            }
            catch
            {
                //Do nothing
            }
        }
    }
}
