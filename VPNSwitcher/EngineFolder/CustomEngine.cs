﻿using MAPrimitives.EngineFolder;

namespace VPNSwitcher.EngineFolder
{
    public class CustomEngine : MAEngine
    {
        //internal bool Ed;
        //:IEngine

        public CustomEngine()
        {
            InputQueueBehavior = new CustomEngineInputQueueBehavior(this);
            Stats = new CustomStats();

            //Queue = new SimpleQueue();
            //(Queue as SimpleQueue).ElementCanBeAddedBehavior = new NonUniqueElementCanBeAddedBehavior();
        }

        public override void InitSpecific()
        {
        }

        public override void SetDL(object dl)
        {
            //(OptionWrapper.Options as CustomOptions).MaxResults = (int) _DL;
        }

        public override void ProcessItemSpecific(object item)
        {
            
        }

        public override bool LimitsReached()
        {
            return false;
        }
    }
}