﻿using MAPrimitives.OptionFolder;

namespace VPNSwitcher.EngineFolder
{
    public class CustomOptions : MAOptions
    {
        public CustomOptions()
        {
            ThreadNumber = 3;
            SwitchVpnEachMin = 30;
            WaitForConnectionSec = 60;
            StopIfVpnNotConnected = true;
            NotifyAboutReconnectingIfMinimized = true;
            PassiveConnectionManagement = true;
        }

        public string ConfigFolder { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int SwitchVpnEachMin { get; set; }
        public int WaitForConnectionSec { get; set; }
        public bool StopIfVpnNotConnected { get; set; }
        public bool NotifyAboutReconnectingIfMinimized { get; set; }
        public bool PassiveConnectionManagement { get; set; }
    }
}