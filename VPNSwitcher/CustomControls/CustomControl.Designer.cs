﻿using DevExpress.XtraEditors;

namespace VPNSwitcher.CustomControls
{
    partial class CustomControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PassiveManagement_checkBox = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.WaitForConnection_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.NotifyAboutReconnectingIfMinimized_checkBox = new System.Windows.Forms.CheckBox();
            this.StopOnErrors_checkBox = new System.Windows.Forms.CheckBox();
            this.Go_button = new System.Windows.Forms.Button();
            this.State_label = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SwitchTime_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.Password_textBox = new System.Windows.Forms.TextBox();
            this.UserName_textBox = new System.Windows.Forms.TextBox();
            this.OvpnFolder_textBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.Indication_pictureBox = new System.Windows.Forms.PictureBox();
            this.NotifyIcon_contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.WaitForConnection_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchTime_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Indication_pictureBox)).BeginInit();
            this.NotifyIcon_contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // PassiveManagement_checkBox
            // 
            this.PassiveManagement_checkBox.AutoSize = true;
            this.PassiveManagement_checkBox.Checked = true;
            this.PassiveManagement_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PassiveManagement_checkBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PassiveManagement_checkBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.PassiveManagement_checkBox.Location = new System.Drawing.Point(123, 178);
            this.PassiveManagement_checkBox.Name = "PassiveManagement_checkBox";
            this.PassiveManagement_checkBox.Size = new System.Drawing.Size(180, 17);
            this.PassiveManagement_checkBox.TabIndex = 32;
            this.PassiveManagement_checkBox.Text = "Passive connection management";
            this.PassiveManagement_checkBox.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(210, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "sec";
            // 
            // WaitForConnection_numericUpDown
            // 
            this.WaitForConnection_numericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WaitForConnection_numericUpDown.Location = new System.Drawing.Point(123, 106);
            this.WaitForConnection_numericUpDown.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.WaitForConnection_numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.WaitForConnection_numericUpDown.Name = "WaitForConnection_numericUpDown";
            this.WaitForConnection_numericUpDown.Size = new System.Drawing.Size(81, 20);
            this.WaitForConnection_numericUpDown.TabIndex = 27;
            this.WaitForConnection_numericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.WaitForConnection_numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(14, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Wait for connection:";
            // 
            // NotifyAboutReconnectingIfMinimized_checkBox
            // 
            this.NotifyAboutReconnectingIfMinimized_checkBox.AutoSize = true;
            this.NotifyAboutReconnectingIfMinimized_checkBox.Checked = true;
            this.NotifyAboutReconnectingIfMinimized_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NotifyAboutReconnectingIfMinimized_checkBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NotifyAboutReconnectingIfMinimized_checkBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.NotifyAboutReconnectingIfMinimized_checkBox.Location = new System.Drawing.Point(123, 155);
            this.NotifyAboutReconnectingIfMinimized_checkBox.Name = "NotifyAboutReconnectingIfMinimized_checkBox";
            this.NotifyAboutReconnectingIfMinimized_checkBox.Size = new System.Drawing.Size(201, 17);
            this.NotifyAboutReconnectingIfMinimized_checkBox.TabIndex = 31;
            this.NotifyAboutReconnectingIfMinimized_checkBox.Text = "Notify about reconnecting if minimized";
            this.NotifyAboutReconnectingIfMinimized_checkBox.UseVisualStyleBackColor = true;
            // 
            // StopOnErrors_checkBox
            // 
            this.StopOnErrors_checkBox.AutoSize = true;
            this.StopOnErrors_checkBox.Checked = true;
            this.StopOnErrors_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StopOnErrors_checkBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StopOnErrors_checkBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.StopOnErrors_checkBox.Location = new System.Drawing.Point(123, 132);
            this.StopOnErrors_checkBox.Name = "StopOnErrors_checkBox";
            this.StopOnErrors_checkBox.Size = new System.Drawing.Size(150, 17);
            this.StopOnErrors_checkBox.TabIndex = 29;
            this.StopOnErrors_checkBox.Text = "Stop if VPN not connected";
            this.StopOnErrors_checkBox.UseVisualStyleBackColor = true;
            // 
            // Go_button
            // 
            this.Go_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Go_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.Go_button.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Go_button.Location = new System.Drawing.Point(8, 210);
            this.Go_button.Name = "Go_button";
            this.Go_button.Size = new System.Drawing.Size(379, 48);
            this.Go_button.TabIndex = 33;
            this.Go_button.Text = "Go";
            this.Go_button.UseVisualStyleBackColor = true;
            this.Go_button.Click += new System.EventHandler(this.Go_button_Click);
            // 
            // State_label
            // 
            this.State_label.AutoSize = true;
            this.State_label.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.State_label.Location = new System.Drawing.Point(34, 264);
            this.State_label.Name = "State_label";
            this.State_label.Size = new System.Drawing.Size(109, 13);
            this.State_label.TabIndex = 34;
            this.State_label.Text = "Wait for connection...";
            this.State_label.TextChanged += new System.EventHandler(this.State_label_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(210, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "min";
            // 
            // SwitchTime_numericUpDown
            // 
            this.SwitchTime_numericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SwitchTime_numericUpDown.Location = new System.Drawing.Point(123, 80);
            this.SwitchTime_numericUpDown.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.SwitchTime_numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SwitchTime_numericUpDown.Name = "SwitchTime_numericUpDown";
            this.SwitchTime_numericUpDown.Size = new System.Drawing.Size(81, 20);
            this.SwitchTime_numericUpDown.TabIndex = 24;
            this.SwitchTime_numericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SwitchTime_numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Password_textBox
            // 
            this.Password_textBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Password_textBox.Location = new System.Drawing.Point(123, 54);
            this.Password_textBox.Name = "Password_textBox";
            this.Password_textBox.PasswordChar = '●';
            this.Password_textBox.Size = new System.Drawing.Size(267, 20);
            this.Password_textBox.TabIndex = 22;
            // 
            // UserName_textBox
            // 
            this.UserName_textBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UserName_textBox.Location = new System.Drawing.Point(123, 28);
            this.UserName_textBox.Name = "UserName_textBox";
            this.UserName_textBox.Size = new System.Drawing.Size(267, 20);
            this.UserName_textBox.TabIndex = 20;
            // 
            // OvpnFolder_textBox
            // 
            this.OvpnFolder_textBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OvpnFolder_textBox.Location = new System.Drawing.Point(123, 2);
            this.OvpnFolder_textBox.Name = "OvpnFolder_textBox";
            this.OvpnFolder_textBox.ReadOnly = true;
            this.OvpnFolder_textBox.Size = new System.Drawing.Size(267, 20);
            this.OvpnFolder_textBox.TabIndex = 18;
            this.OvpnFolder_textBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OvpnFolder_textBox_MouseClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(26, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Switch VPN each:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(61, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Password:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(56, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "User name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(8, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Folder with ovpn files:";
            // 
            // Indication_pictureBox
            // 
            this.Indication_pictureBox.Image = global::VPNSwitcher.Properties.Resources.grey;
            this.Indication_pictureBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Indication_pictureBox.Location = new System.Drawing.Point(8, 261);
            this.Indication_pictureBox.Name = "Indication_pictureBox";
            this.Indication_pictureBox.Size = new System.Drawing.Size(16, 16);
            this.Indication_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Indication_pictureBox.TabIndex = 30;
            this.Indication_pictureBox.TabStop = false;
            // 
            // NotifyIcon_contextMenuStrip
            // 
            this.NotifyIcon_contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.NotifyIcon_contextMenuStrip.Name = "NotifyIcon_contextMenuStrip";
            this.NotifyIcon_contextMenuStrip.Size = new System.Drawing.Size(106, 54);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.ShowToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(102, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // CustomControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PassiveManagement_checkBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.WaitForConnection_numericUpDown);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Indication_pictureBox);
            this.Controls.Add(this.NotifyAboutReconnectingIfMinimized_checkBox);
            this.Controls.Add(this.StopOnErrors_checkBox);
            this.Controls.Add(this.Go_button);
            this.Controls.Add(this.State_label);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.SwitchTime_numericUpDown);
            this.Controls.Add(this.Password_textBox);
            this.Controls.Add(this.UserName_textBox);
            this.Controls.Add(this.OvpnFolder_textBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "CustomControl";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Size = new System.Drawing.Size(398, 286);
            this.Load += new System.EventHandler(this.CustomControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.WaitForConnection_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchTime_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Indication_pictureBox)).EndInit();
            this.NotifyIcon_contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox PassiveManagement_checkBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown WaitForConnection_numericUpDown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox Indication_pictureBox;
        private System.Windows.Forms.CheckBox NotifyAboutReconnectingIfMinimized_checkBox;
        private System.Windows.Forms.CheckBox StopOnErrors_checkBox;
        private System.Windows.Forms.Button Go_button;
        private System.Windows.Forms.Label State_label;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown SwitchTime_numericUpDown;
        private System.Windows.Forms.TextBox Password_textBox;
        private System.Windows.Forms.TextBox UserName_textBox;
        private System.Windows.Forms.TextBox OvpnFolder_textBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ContextMenuStrip NotifyIcon_contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;


    }
}
