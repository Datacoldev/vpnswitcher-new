﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using MAForms;
using MAForms.UI;
using MAPrimitives.OptionFolder;
using VPNSwitcher.EngineFolder;
using VPNSwitcher.EngineFolder.PackerFolder;
using VPNSwitcher.Properties;
using Timer = System.Threading.Timer;

namespace VPNSwitcher.CustomControls
{
    public partial class CustomControl : UserControl, IMainControl
    {
        [DllImport("shell32.dll", EntryPoint = "IsUserAnAdmin")]
        public static extern bool IsUserAdministrator();

        private RunState _runState = RunState.Stopped;
        private string _openVpnExecutable = "";
        private Timer _mainTimer;
        private OpenVpnManager _vpnManager;
        private readonly Random _random = new Random();

        private Options _options;

        private MainFormTemplate _form;

        public CustomControl()
        {
            InitializeComponent();
        }

        public Options Options
        {
            get
            {
                var customOptions = _options as CustomOptions;
                if (customOptions != null)
                {
                    customOptions.ConfigFolder = OvpnFolder_textBox.Text;
                    customOptions.UserName = UserName_textBox.Text;
                    customOptions.Password = Password_textBox.Text;
                    customOptions.SwitchVpnEachMin = (int) SwitchTime_numericUpDown.Value;
                    customOptions.WaitForConnectionSec = (int) WaitForConnection_numericUpDown.Value;
                    customOptions.StopIfVpnNotConnected = StopOnErrors_checkBox.Checked;
                    customOptions.NotifyAboutReconnectingIfMinimized = NotifyAboutReconnectingIfMinimized_checkBox.Checked;
                    customOptions.PassiveConnectionManagement = PassiveManagement_checkBox.Checked;
                }

                return _options;
            }
            set
            {
                _options = value;
                UpdateOptions();
            }
        }

        private void UpdateOptions()
        {
            var customOptions = _options as CustomOptions;
            if (customOptions != null && customOptions.IsLoaded)
            {
                OvpnFolder_textBox.Text = customOptions.ConfigFolder;
                UserName_textBox.Text = customOptions.UserName;
                Password_textBox.Text = customOptions.Password;
                SwitchTime_numericUpDown.Value = customOptions.SwitchVpnEachMin;
                WaitForConnection_numericUpDown.Value = customOptions.WaitForConnectionSec;
                StopOnErrors_checkBox.Checked = customOptions.StopIfVpnNotConnected;
                NotifyAboutReconnectingIfMinimized_checkBox.Checked = customOptions.NotifyAboutReconnectingIfMinimized;
                PassiveManagement_checkBox.Checked = customOptions.PassiveConnectionManagement;
            }
        }

        private void CustomControl_Load(object sender, EventArgs e)
        {
            var form = Parent.Parent as MainFormTemplate;
            if (form != null)
            {
                var isUserAdmin = IsUserAdministrator();

                if (!isUserAdmin)
                {
                    MessageBox.Show(@"This program must be run as an administrator!", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                    return;
                }

                _form = form;
                form.MaximizeBox = false;
                form.Resize += Form1_Resize;
                form.FormClosing += Form1_FormClosing;
                form.ResizeEnd += form_ResizeEnd;


                form.Size = _formSize;
                form.MinimumSize = _formSize;
                form.MaximumSize = _formSize;

                form.notifyIcon.Visible = true;
            }
        }

        void form_ResizeEnd(object sender, EventArgs e)
        {
            _form.Size = _formSize;
        }

        private void OvpnFolder_textBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (folderBrowserDialog.ShowDialog(this) == DialogResult.OK)
			{
				if (Directory.GetFiles(folderBrowserDialog.SelectedPath, "*.ovpn").Length == 0)
				{
				    if (MessageBox.Show(this, string.Format("Folder \"{0}\" not contain ovpn files. Continue using this folder?", folderBrowserDialog.SelectedPath), @"No files", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				    {
				        OvpnFolder_textBox.Text = folderBrowserDialog.SelectedPath;
				    }
				}
				else
				{
					OvpnFolder_textBox.Text = folderBrowserDialog.SelectedPath;
				}

				Settings.Default.Save();
			}
        }

        /// <summary>
		/// Check tap adapter is up
		/// </summary>
		/// <returns></returns>
		private bool TapAdapterIsUp()
		{
			var retval = false;
			var adapters = NetworkInterface.GetAllNetworkInterfaces();
			foreach (var adapter in adapters)
			{
				if (adapter.Description.StartsWith("TAP-Windows Adapter V9"))
				{
					retval = adapter.OperationalStatus == OperationalStatus.Up;
				}
			}
			return retval;
		}

		/// <summary>
		/// Stop executing current process
		/// </summary>
		private void StopExecution()
		{
			if (InvokeRequired)
			{
				Invoke((MethodInvoker)(StopExecution));
			}
			else
			{
				if (_form.WindowState == FormWindowState.Minimized)
				{
					_form.WindowState = FormWindowState.Normal;
					_form.ShowInTaskbar = true;
				}
				if (_mainTimer != null)
				{
					_mainTimer.Change(Timeout.Infinite, Timeout.Infinite);
					_mainTimer.Dispose();
					_mainTimer = null;
				}
				KillOpenVpnIfExists();
				foreach (Control control in Controls) { control.Enabled = true; }
				Go_button.Text = @"Go";
				_runState = RunState.Stopped;
				State_label.Text = @"Wait for connection...";
				Indication_pictureBox.Image = Resources.grey;
			}
		}

		/// <summary>
		/// Go button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Go_button_Click(object sender, EventArgs e)
		{
			//If we in run state stop all
			if (_runState == RunState.Running)
			{
				StopExecution();
				return;
			}
			//Check all conditions
			//Folder is empty
			if (String.IsNullOrEmpty(OvpnFolder_textBox.Text))
			{
				MessageBox.Show(this, @"Please specify folder with ovpn files.", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			//Folder with VPN files not exists
			if (!Directory.Exists(OvpnFolder_textBox.Text))
			{
			    MessageBox.Show(this, string.Format("Folder \"{0}\" not exists.", OvpnFolder_textBox.Text), @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			//ovpn files not exists
			if (Directory.GetFiles(OvpnFolder_textBox.Text, "*.ovpn").Length == 0)
			{
				if (MessageBox.Show(this, string.Format("Folder \"{0}\" not contain ovpn files. Continue using this folder?", OvpnFolder_textBox.Text), @"No files", MessageBoxButtons.OK, MessageBoxIcon.Warning) != DialogResult.Yes)
				{
					return;
				}
			}

            ////User name is empty
            //if (String.IsNullOrEmpty(UserName_textBox.Text))
            //{
            //    MessageBox.Show(this, @"Please specify user name for VPN connection.", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            ////Password is empty
            //if (String.IsNullOrEmpty(UserName_textBox.Text))
            //{
            //    MessageBox.Show(this, @"Please specify password for VPN connection.", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}

			Settings.Default.Save();
			//Check TAP device
			if (!CheckAndInstallTapDevice())
			{
				MessageBox.Show(this, @"Unable to get TAP network adapter. Stop execution.", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			//Check and get openVPN
			_openVpnExecutable = GetOpenVpnExecutable();
			if (String.IsNullOrEmpty(_openVpnExecutable))
			{
				MessageBox.Show(this, @"Unable to get openVPN executable. Stop execution.", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			Settings.Default.Save();
			//All preparation done. Start working timer
			foreach (Control control in Controls) { control.Enabled = false; }
			Go_button.Enabled = true;
			State_label.Enabled = true;
			Go_button.Text = @"Stop";
			_runState = RunState.Running;
			_mainTimer = new Timer(MainTimer, null, 0, (int)SwitchTime_numericUpDown.Value * 60 * 1000);
		}

		/// <summary>
		/// Detect TAP device exists
		/// </summary>
		/// <returns></returns>
		private bool TapAdapterExist()
		{
			var retval = false;
			var interfaces = NetworkInterface.GetAllNetworkInterfaces();
			foreach (var netInterface in interfaces)
			{
				if (netInterface.Description.StartsWith("TAP-Windows Adapter V9"))
				{
					retval = true;
					break;
				}
			}
			return retval;
		}

		/// <summary>
		/// Check and install TAP device
		/// </summary>
		private bool CheckAndInstallTapDevice()
		{
			var retval = true;
			//Check TAP adapter exist and install it if need
			if (!TapAdapterExist())
			{
				if (!IsUserAdministrator())
				{
					if (MessageBox.Show(this, @"We can't detect installed TAP adapter. For install TAP adapter driver we need administrative privileges. Start TAP adapter installation right now?", @"Install TAP adapter", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
					{
						var process = new Process()
						{
							StartInfo = new ProcessStartInfo()
							{
								FileName = Application.ExecutablePath,
								Arguments = "/installtap",
								Verb = "runas"
							}
						};
						process.Start();
						process.WaitForExit();
					}
				}
				else
				{
					try
					{
						var installTapAdapterProcess = InstallTapAdapter();
						if (installTapAdapterProcess != null && !installTapAdapterProcess.HasExited)
						{
							State_label.Text = @"Wait while TAP Adapter installed...";
							//Wait while openVPN installed
							installTapAdapterProcess.WaitForExit();
						}
					}
					catch (Exception ex)
					{
						MessageBox.Show(this, string.Format("Unable to install TAP Adapter. Error:{0}\r\nTry restart PC and run this application again.", ex.Message), @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
						State_label.Text = @"Wait for connection...";
						retval = false;
					}

					//Check TAP again after install
					if (!TapAdapterExist())
					{
						MessageBox.Show(this, @"No TAP-Windows network interface installed. Please try install TAP Adapter manually.", @"No TAP-device", MessageBoxButtons.OK, MessageBoxIcon.Error);
						State_label.Text = @"Wait for connection...";
						retval = false;
					}
				}
			}
			return retval;
		}

		/// <summary>
		/// Run OpenVPN installation
		/// </summary>
		/// <returns></returns>
		private Process InstallTapAdapter()
		{
		    var installerName = "tap-vista.pkg";
			if (!Environment.Is64BitOperatingSystem)
			{
				installerName = "tap-vista32.pkg";
			}
			if (Environment.OSVersion.Version.Major == 5)
			{
				//For XP
				installerName = "tap-xp.pkg";
			}

			string programFiles;
			if (Environment.OSVersion.Version.Major == 5)
			{
				programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
			}
			else
			{
				if (Environment.Is64BitOperatingSystem)
				{
					programFiles = Environment.ExpandEnvironmentVariables("%ProgramW6432%");
				}
				else
				{
					programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
				}
			}
			programFiles = Path.Combine(programFiles, "TAP-Windows");
			if (!Directory.Exists(programFiles)) { Directory.CreateDirectory(programFiles); }
			var unpackedPackageFileName = Path.Combine(Path.GetTempPath(), installerName);
			var assembly = Assembly.GetExecutingAssembly();
			using (var stream = assembly.GetManifestResourceStream(String.Format("ovpnswitcher.{0}", installerName)))
			{
				using (var fileStream = new FileStream(unpackedPackageFileName, FileMode.Create))
				{
				    if (stream != null)
				    {
				        stream.CopyTo(fileStream);
				        stream.Close();
				    }
				}
			}

			var packer = new Packer(unpackedPackageFileName);
			packer.Open();
			packer.UnpackAllToFolder(programFiles);
			packer.Close();
			try
			{
				File.Delete(unpackedPackageFileName);
			}
			catch
			{
				//Do nothing
			}
			//Preparing command line
			var installExecutable = Path.Combine(programFiles, "tapinstall.exe");
			var arguments = String.Format("install \"{0}\" tap0901", Path.Combine(programFiles, "OemVista.inf"));
			//If we have XP
			if (Environment.OSVersion.Version.Major == 5)
			{
				installExecutable = Path.Combine(programFiles, "devcon.exe");
				arguments = String.Format("install \"{0}\" tap0901", Path.Combine(programFiles, "OemWin2k.inf"));
			}
			var startInfo = new ProcessStartInfo()
			{
				FileName = installExecutable,
				Arguments = arguments,
				WorkingDirectory = programFiles,
				CreateNoWindow = true,
				UseShellExecute = false
			};

			var process = Process.Start(startInfo);
			return process;
		}


		/// <summary>
		/// Get openVPN executable
		/// </summary>
		/// <returns></returns>
		private string GetOpenVpnExecutable()
		{
			//First of all try to kill openvpn process
			while (Process.GetProcessesByName("openvpn").Length > 0)
			{
				Process.GetProcessesByName("openvpn")[0].Kill();
				Thread.Sleep(50);
			}

		    //64bit driver for Vista+
			var packageName = "ovpn_vista.pkg";
			if (!Environment.Is64BitOperatingSystem)
			{
				//Vista+ 32 bit
				packageName = "ovpn_vista32.pkg";
			}
			if (Environment.OSVersion.Version.Major == 5)
			{
				//XP 32 bit
				packageName = "ovpn_xp.pkg";
			}
			//Unpack 
			var assembly = Assembly.GetExecutingAssembly();
            var resourceName = String.Format("VPNSwitcher.{0}", packageName);
			var unpackPath = Path.Combine(Path.GetTempPath(), "ovpn");
			if (!Directory.Exists(unpackPath))
			{
				Directory.CreateDirectory(unpackPath);
			}
			var installFile = Path.Combine(unpackPath, packageName);
			using (var stream = assembly.GetManifestResourceStream(resourceName))
			{
				using (var fileStream = new FileStream(installFile, FileMode.Create))
				{
				    if (stream != null)
				    {
				        stream.CopyTo(fileStream);
				        stream.Close();
				    }
				}
			}
			var packer = new Packer(installFile);
			packer.Open();
			packer.UnpackAllToFolder(unpackPath);
			Thread.Sleep(500);
			var retval = Path.Combine(unpackPath, "openvpn.exe");
			return retval;
		}

		/// <summary>
		/// Kill openVPN process if exists
		/// </summary>
		private void KillOpenVpnIfExists()
		{
			if (_vpnManager != null)
			{
				_vpnManager.Dispose();
				Thread.Sleep(100);
			}
		}

		/// <summary>
		/// Main timer routine
		/// </summary>
		/// <param name="state"></param>
		private void MainTimer(object state)
		{
			if (_runState != RunState.Running) { return; }
			_form.notifyIcon.Icon = Resources.grey_ico;
			Indication_pictureBox.Invoke((MethodInvoker)(() => { Indication_pictureBox.Image = Resources.grey; }));
			//Select ovpn file
			var ovpnFiles = Directory.GetFiles(OvpnFolder_textBox.Text, "*.ovpn");
			if (ovpnFiles.Length == 0)
			{
				_mainTimer.Change(Timeout.Infinite, Timeout.Infinite);
				StopExecution();
				Invoke((MethodInvoker)(() => { MessageBox.Show(this, string.Format("Unable to get any ovpn file from \"{0}\". Stop execution.", OvpnFolder_textBox.Text), @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }));
				return;
			}
			//Get random file
			var random = new Random();
			var workingOvpnFile = ovpnFiles[random.Next(0, ovpnFiles.Length)];
			var connectionName = Path.GetFileNameWithoutExtension(workingOvpnFile);
			//Kill openVPN
			KillOpenVpnIfExists();
			_vpnManager = new OpenVpnManager("127.0.0.1", 12000, workingOvpnFile, UserName_textBox.Text, Password_textBox.Text, _openVpnExecutable, !PassiveManagement_checkBox.Checked, false);
			var stopWatch = new Stopwatch();
			stopWatch.Start();
			var vpnConnected = false;
			var currentIp = "127.0.0.1";
			//Wait some time for connect
			while (stopWatch.ElapsedMilliseconds < (int)WaitForConnection_numericUpDown.Value * 1000)
			{
				if (PassiveManagement_checkBox.Checked)
				{
					Invoke((MethodInvoker)(() => { State_label.Text = string.Format("{0}: Connecting...", connectionName); }));
					if (TapAdapterIsUp())
					{
						currentIp = GetIp();
						Invoke((MethodInvoker)(() => { State_label.Text = string.Format("{0}: CONNECTED ({1})", connectionName, currentIp); }));
						vpnConnected = true;
						_form.notifyIcon.Icon = Resources.green_ico;
						Indication_pictureBox.Invoke((MethodInvoker)(() => { Indication_pictureBox.Image = Resources.green; }));
						break;
					}

					Thread.Sleep(500);
				}
				else
				{
					var vpnState = _vpnManager.GetState();
					if (vpnState.Contains("CONNECTED,SUCCESS"))
					{
						currentIp = GetIp();
						Invoke((MethodInvoker)(() => { State_label.Text = string.Format("{0}: CONNECTED ({1})", connectionName, currentIp); }));
						vpnConnected = true;
						_form.notifyIcon.Icon = Resources.green_ico;
						Indication_pictureBox.Invoke((MethodInvoker)(() => { Indication_pictureBox.Image = Resources.green; }));
						break;
					}

					Thread.Sleep(500);
					var states = vpnState.Split(',');
					if (states.Length > 1)
					{
						Invoke((MethodInvoker)(() => { State_label.Text = string.Format("{0}: {1}", connectionName, states[1]); }));
					}
					if (_runState != RunState.Running) { break; }
				}
			}
			stopWatch.Stop();
			if (_runState != RunState.Running)
			{
				StopExecution();
				return;
			}
			if (!vpnConnected)
			{
				if (StopOnErrors_checkBox.Checked)
				{
					StopExecution();
					Invoke((MethodInvoker)(() => { MessageBox.Show(this, string.Format("Unable to connect with \"{0}\". Stop execution.", workingOvpnFile), @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }));
				}
			}
			else
			{
				if (_form.WindowState == FormWindowState.Minimized)
				{
					if (NotifyAboutReconnectingIfMinimized_checkBox.Checked)
					{
					    _form.notifyIcon.ShowBalloonTip(1000, "VPN Switcher", string.Format("Connected to: {0}\r\nIP: {1}", connectionName, currentIp), ToolTipIcon.Info);
					}
				}
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			StopExecution();
		}

        private readonly Size _formSize = new Size(420, 374);

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (_form.WindowState == FormWindowState.Minimized)
            {
                _form.ShowInTaskbar = false;
                _form.notifyIcon.Visible = true;
                _form.notifyIcon.ShowBalloonTip(1000, "VPN Switcher", "VPN Switcher hide in system tray", ToolTipIcon.Info);
            }
        }

        /// <summary>
		/// Show main window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ShowToolStripMenuItem_Click(object sender, EventArgs e)
		{
			_form.ShowInTaskbar = true;
			_form.WindowState = FormWindowState.Normal;
			_form.notifyIcon.Visible = false;
		}

		/// <summary>
		/// Close application
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			StopExecution();
			_form.Close();
		}

        //private void Form1_Load(object sender, EventArgs e)
        //{
        //    //If just install tap adapter - do it and close
        //    if (_args.Length == 1 && _args[0] == "/installtap")
        //    {
        //        InstallTAPAdapt();
        //        this.Close();
        //    }
        //}

		/// <summary>
		/// Update tool tip icon
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void State_label_TextChanged(object sender, EventArgs e)
		{
			if (State_label.Text.Length > 64)
			{
				_form.notifyIcon.Text = State_label.Text.Substring(0, 63);
			}
			else
			{
				_form.notifyIcon.Text = State_label.Text;
			}
		}

		/// <summary>
		/// Get current IP
		/// </summary>
		/// <returns></returns>
		private string GetIp()
		{
			var retval = "Unknown";
			try
			{
				using (var client = new WebClient())
				{
					//using random seed for prevent caching
				    retval = client.DownloadString(new Uri(string.Format("https://api.ipify.org/?some={0}", _random.Next())));
				}
			}
			catch
			{
				//Do nothing
			}
			return retval;
		}
    }
}