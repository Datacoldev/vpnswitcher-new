﻿//#define DEVMODE

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Windows.Forms;
using MAForms.ExtraForms;
using MAForms.Misc;
using MAForms.Misc.ButtonGenerationFolder;
using MAPrimitives.Misc;
using VPNSwitcher.CustomControls;
using VPNSwitcher.CustomLocalization.CustomLocalizationResources;
using VPNSwitcher.EngineFolder;
using VPNSwitcher.Misc;

namespace VPNSwitcher
{
    internal static class Program
    {
        private static readonly StarterParameters StarterParameters = new StarterParameters();

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            LoadResolver();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

#if DEVMODE
            const bool devMode = true;
#else
            const bool devMode = false;
#endif

            var hardcodedParameters = new HardcodedParameters(
                CustomStrings.AppName, "1.0", "app",
                CustomCommon.SiteOfProduct, "lic.txt",
                devMode, 30, CustomCommon.DemoResults);

            hardcodedParameters.ASupported = false;
            //
            //hardcodedParameters.ComponentInstallerRules.Add(new DownloadComponentInstallerRule
            //    (false, 2000, "%APPDATA%\\BrowserAgent", "http://parser-yandex-kart.ru/somefolder.zip", "BrowserAgent",1.4));
            //    //"http://traceprice.net/downloads/BrowserAgent.zip"));

            StarterParameters.HardcodedParameters = hardcodedParameters;
            StarterParameters.ShowLanguageSwitcher = false;
            //

            StarterParameters.RunAsAdministrator = true;

            StarterParameters.CMDParameters = new CMDParameters(args); //todo
            StarterParameters.ExePath = Assembly.GetExecutingAssembly().Location;
            StarterParameters.CustomOptionType = typeof (CustomOptions);
            StarterParameters.EngineMainPrimitivesFactory = new CustomEngineMainPrimitivesFactory();
            var assemblyLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (assemblyLocation == null)
            {
                throw new ArgumentNullException(@"assemblyLocation");
            }

            StarterParameters.ApplicationGlyphImagePath = Path.Combine(assemblyLocation, "Images", "app_glyph.png");
            StarterParameters.ApplicationTrayIconPath = Path.Combine(assemblyLocation, "Images", "app_icon.ico");

            StarterParameters.BuyButtonHandler = BuyButtonHandler;
            StarterParameters.HideRibbon = true;
            
            StarterParameters.ButtonGenerationInfoList = new List<ButtonGenerationInfo>();
            StarterParameters.MainControlType = typeof (CustomControl);
            
            var starter = new StarterForm(StarterParameters);
            Application.Run(starter);
        }

        private static void BuyButtonHandler(object sender, EventArgs e)
        {
            MessageBox.Show(CustomCommon.ContactEmailForPurchase + CustomCommon.EmailForPurchase);
        }

        /// <summary>
        /// Embed library resolves
        /// </summary>
        private static void LoadResolver()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolver;
        }

        /// <summary>
        /// Resolve embed dll's
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static Assembly CurrentDomain_AssemblyResolver(object sender, ResolveEventArgs args)
        {
            var resourceName = Assembly.GetExecutingAssembly().FullName.Split(',').First() + "." + new AssemblyName(args.Name).Name + ".dll";
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            {
                if (stream != null)
                {
                    var assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            }
            return null;
        }
    }
}